import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import {FormGroup, FormBuilder, FormControl, Validators, AbstractControl} from '@angular/forms'
import {SignatureService} from './assinatura.service'
import {tap} from 'rxjs/operators'
declare var BryApiModule: any;



@Component({
  selector: 'cms-assinatura',
  templateUrl: './assinatura.component.html',
  styleUrls: ['./assinatura.component.css']
})
export class InitializeComponent implements OnInit {


  signatureForm: FormGroup;

  inicializationResult;

  finalizationResult;

  file: any;

  signedAttributes;

  base64Certificate;

  title =  'CMS Signature';

  auxSignatureValue;

  signature;

  token;

  nonce;

  documentNonce;

  constructor(private signatureService: SignatureService,
              private router: Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.signatureForm = this.formBuilder.group({
      token:  new FormControl('', { validators: [Validators.required], updateOn: 'blur'}),
      attached: new FormControl('true', { validators: [Validators.required], updateOn: 'blur'}),
      profile: new FormControl('BASIC', { validators: [Validators.required], updateOn: 'blur'}),
      hashAlgorithm: new FormControl('SHA256', { validators: [Validators.required], updateOn: 'blur'}),
      certificate: new FormControl(''),
      operationType: new FormControl('SIGNATURE', { validators: [Validators.required], updateOn: 'blur'}),
      file: this.formBuilder.control(null, [Validators.required]),
      signature: new FormControl('')
    })

  }

  initialize(signatureForm: FormGroup, base64Certificate) {

    this.token = this.signatureForm.get('token').value;

    this.base64Certificate = base64Certificate;

    this.nonce = this.signatureService.generateRandonNumber();
    console.log('Nonce', this.nonce);

    this.documentNonce = this.signatureService.generateRandonNumber();
    console.log('Document nonce', this.documentNonce);

    this.signatureService.initialize(this.nonce, this.documentNonce, signatureForm, this.base64Certificate, this.file, this.token)
      .pipe(
        tap((result: any) => {
          this.inicializationResult = result;
        }
        )
      ).subscribe( async (result: any) => {

        this.signedAttributes = result.signedAttributes[0].content ;

        console.log(`Inicialization result: ${this.signedAttributes}`);

        const response = {
          algoritmoHash: "SHA256",
          formatoDadosEntrada: "Base64",
          formatoDadosSaida: "Base64",
          assinaturas: result.assinaturasInicializadas.map((signature: any) => {
            // Directly assign the messageDigest from the first element to hashes
            const hashes = (signature.messageDigest) ? [signature.messageDigest] : [""]; // Use messageDigest from the signature
            
            return {
              hashes, // Add the hashes array to each signature
            };
          }),
        };
      
        const element = document.getElementById("select-certificado-list") as HTMLSelectElement | null;


        console.log('Dados preparados da extensao:', response);

        let signedData;

        await BryApiModule.sign(element.value, JSON.stringify(response))
        .then(async signature => {
          signedData = signature
        });


        console.log('Dados assinados pela extensao: ', signedData);

        var objPreparedExtensionData = JSON.parse(signedData);

        this.auxSignatureValue = objPreparedExtensionData.assinaturas[0].hashes[0];

        console.log('Encrypted signed attributes and Base64 encoded: ', this.auxSignatureValue);

        this.signatureService.finalize(this.nonce, this.documentNonce, signatureForm, this.base64Certificate, this.file, this.signedAttributes, this.auxSignatureValue, this.token)
            .pipe(
                 tap((result: any) => {
                  this.finalizationResult = result;
                  }
            )
        ).subscribe((result: any) => {

          //console.log(`Finalization result: ${result.signatures[0].content}`);

          this.signature = JSON.stringify(result)

          this.signatureForm.get('signature').setValue(this.signature);

          alert('Signature realized successful!');

        })

      } )

  }

  clear(){
    this.signature = null;
    this.file = null;
    this.signatureForm.get('file').reset()
    this.signatureForm.get('file').setValue(null);
    this.signatureForm.get('hashAlgorithm').setValue('SHA256');
  }


  fillCertificateDataForm() {
    BryApiModule.fillCertificateDataForm();
  }

  listCertificates() {
    BryApiModule.listCertificates();
  }


  /*
  servicoDisponivel(){
    let response;
    this.signatureService.isAvailable(this.token)
      .pipe(
        tap((result: any) => {
          response = result;
        }
        )
      ).subscribe((result: any) => {
        console.log(`Resultado da inicialização: ${response}`);
      })
  }
  */

  upload(event){
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }


}
