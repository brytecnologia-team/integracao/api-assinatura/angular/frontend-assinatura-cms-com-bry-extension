import {Injectable} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {FormGroup} from '@angular/forms'


@Injectable()
export class SignatureService{

    URL_INITIALIZE_CMS_SERVER  = '/api/initialize';
    URL_FINALIZE_CMS_SERVER  = '/api/finalize';
    URL_IS_AVAILABLE_CMS_SERVER = '/api/cms-signature-service/v1/isAvailable';


    //result;

    constructor(private http: HttpClient){}


    initialize(nonce, documentNonce, assinaturaForm: FormGroup, base64Certificate, file, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', `Bearer ${token}`)
        const formData = new FormData();
        formData.append('nonce', nonce);
        formData.append('attached', assinaturaForm.get('attached').value );
        formData.append('profile', assinaturaForm.get('profile').value );
        formData.append('hashAlgorithm', assinaturaForm.get('hashAlgorithm').value );
        formData.append('certificate', base64Certificate );
        formData.append('operationType', assinaturaForm.get('operationType').value );
        formData.append('documentNonce', documentNonce);
        formData.append("file", file );

        return this.http.post<any>(this.URL_INITIALIZE_CMS_SERVER, formData, {headers: headersValues}).pipe(map( result => result));
    }

    finalize(nonce, documentNonce, assinaturaForm: FormGroup, base64Certificate, file, signedAttributes, signatureValue, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', `Bearer ${token}`);
        const formData = new FormData();
        formData.append('nonce', nonce);
        formData.append('attached', assinaturaForm.get('attached').value );
        formData.append('profile', assinaturaForm.get('profile').value );
        formData.append('hashAlgorithm', assinaturaForm.get('hashAlgorithm').value );
        formData.append('certificate', base64Certificate );
        formData.append('operationType', assinaturaForm.get('operationType').value );
        formData.append('finalizationNonce', documentNonce );
        formData.append('finalizationSignedAttributes', signedAttributes );
        formData.append('finalizationSignatureValue', signatureValue );
        formData.append('file', file );

        return this.http.post<any>(this.URL_FINALIZE_CMS_SERVER, formData, {headers: headersValues}).pipe(map( result => result));

    }

    isAvailable(token): Observable<any>{
        let headers = new HttpHeaders();
        headers = headers.set('Authorization', `Bearer ${token}`);
        headers = headers.set('Content-Type', 'application/json');

        return this.http.get<any>(this.URL_IS_AVAILABLE_CMS_SERVER, {headers: headers}).pipe(map( result => result));
    }

    generateRandonNumber(): number {
        //Return a random number between 1 and 1000:
        return Math.floor((Math.random() * 1000) + 1);
    }


}
